# Car Battery Monitor Using Wemos D1 Mini and Home Assistant

In this tutorial, I will show you how to use a wemos d1 mini, a power shiled and some resistors to wirelessly monitor your car battery voltage and send the readings to Home Assistant over MQTT.

[![Monitor your car battery youtube video](https://img.youtube.com/vi/VnGRFwDrLHo/maxresdefault.jpg)](https://youtu.be/VnGRFwDrLHo)


## Components used (Amazon links - affiliate):
* Wemos D1 Mini - [US] https://amzn.to/2N7n9zr  [Canada] https://amzn.to/3cpOxDe
* Power Shield - [US]  https://amzn.to/3hCwm0D  [Canada]  https://amzn.to/2XuoIxD
* Assorted Resistors - [US] https://amzn.to/2YGHIbw  [Canada]  https://amzn.to/2AtfU1P


## Home Assistant Code
```yaml
- platform: mqtt
   unique_id: car-elantra-battery
   state_topic: "tele/carbattery/SENSOR"
   name: car_elantra_battery
   unit_of_measurement: "v"
   value_template: "{{ (value_json.ANALOG.Range | float / 100 | round(2) )}}"
   qos: 2
```

## Steps

Having the ability to monitor your car battery can prevent some unpleasant surprises. I will show you how I assembled the hardware, loaded the software and installed the monitor in my car.

You will need a wemos d1 mini, a power shield and some resistors. First, I started by removing the power plug and installed the smaller connector to make the hardware more compact.

The D1 mini can measure external voltage up to 3.3v by using a voltage divider using R1=220KΩ & R2=100KΩ . This the voltage within the 0-1 Volt that the ADC can tolerate. To increase the 3.3v to 16v needed for the car battery, we need to increase R1 to 1.44MΩ. To do that we can add another 1.22MΩ in series to get the total 1.44MΩ. I did this by soldering a 1MΩ resistor to a 220KΩ resistor as shown here.

<img alt="Wiring Diagram" src="https://gitlab.com//ontk/home-assistant-car-battery-monitor/-/raw/master/wiring_diagram.png"  width="480" >

I then connected the D1 mini to my laptop and load the software. Make sure you select the sensor.bin version for the Analog input functionality. I then resumed with a typical Tasmota configuration.

I attached a long wire to the power input terminals to be able to connect them to the car battery.

At the car, I opened the hood and located the fuse box. I found the fuse box to be a safe and secure place to install my device.I first wrapped the device in heat resistant tape to cover any exported pin to withstand the engine heat. Since the entire car chase is ground, I found the nearest screw and connected my ground to it. Next, I located the nearest connection to the battery positive rail and connected my positive power input to it. 

I then proceeded with calibrating the range of the analog input. I did this by connecting a multimeter to the battery and read the current voltage (in my case it was 12.73 volts). I then did some trial and error until my reading was 1273 as the analog reading. The last step was closing the fuse box and the car hood.

Back in Home Assistant, I opened the configuration file and added a new MQTT sensor. After I saved, I restarted home assistant for it to take effect. When it came back online, I added the new sensor to the dashboard.

My integration is now complete. You can now use this sensor to trigger alerts!

